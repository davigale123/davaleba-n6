package com.example.myapplication

import adapters.ViewPageFragmentAdapter
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {

    private lateinit var viewPager2: ViewPager2
    private lateinit var tabLayout: TabLayout
    private lateinit var viewPageFragmentAdapter: ViewPageFragmentAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tabLayout = findViewById(R.id.bar)
        viewPager2 = findViewById(R.id.pages)
        viewPageFragmentAdapter = ViewPageFragmentAdapter(this)

        viewPager2.adapter = viewPageFragmentAdapter

        TabLayoutMediator(tabLayout, viewPager2) { tab, position ->
            tab.text = "Page ${position + 1}"
            when (position) {
                0 -> tab.setIcon(R.drawable.ic_baseline_elderly_24)
                1 -> tab.setIcon(R.drawable.ic_baseline_accessible_forward_24)
                2 -> tab.setIcon(R.drawable.ic_baseline_airline_seat_flat_24)
            }

        }.attach()

    }
}